# SwiftNDNTouchConsumer

## [![CI Status](http://img.shields.io/travis/Jongdeog Lee/SwiftNDNTouchConsumer.svg?style=flat)](https://travis-ci.org/Jongdeog Lee/SwiftNDNTouchConsumer)
[![Version](https://img.shields.io/cocoapods/v/SwiftNDNTouchConsumer.svg?style=flat)](http://cocoapods.org/pods/SwiftNDNTouchConsumer)
[![License](https://img.shields.io/cocoapods/l/SwiftNDNTouchConsumer.svg?style=flat)](http://cocoapods.org/pods/SwiftNDNTouchConsumer)
[![Platform](https://img.shields.io/cocoapods/p/SwiftNDNTouchConsumer.svg?style=flat)](http://cocoapods.org/pods/SwiftNDNTouchConsumer)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SwiftNDNTouchConsumer is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "SwiftNDNTouchConsumer"
```

## Author

Jongdeog Lee, jdlee6461@gmail.com

## License

SwiftNDNTouchConsumer is available under the MIT license. See the LICENSE file for more info.
