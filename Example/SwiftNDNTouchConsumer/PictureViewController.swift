//
//  ViewController.swift
//  SwiftNDNTouchConsumer
//
//  Created by Jongdeog Lee on 12/01/2016.
//  Copyright (c) 2016 Jongdeog Lee. All rights reserved.
//

import UIKit
import SwiftNDNTouchConsumer
import ActionSheetPicker_3_0
import CoreLocation

class PictureViewController: UITableViewController, UIGestureRecognizerDelegate, CLLocationManagerDelegate {
    
    var locationManager: CLLocationManager!
    var prefix = Name(url: rootPrefix)
    
    var index = [String]()
    var lastSelectedCell: Int?
    var receivedData: UInt64?
    var totalData: UInt64?
    var downloadAlertController: UIAlertController?
    var progressDownload: UIProgressView?
    
    var isFirst = true
    var prevForwarder: Forwarder?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        receivedData = 0
        
        // let title = prefix?.toUri().replacingOccurrences(of: appPrefix, with: "")        
        let addButton1 = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector (insertNewObject(_:)))
        self.navigationItem.rightBarButtonItem = addButton1
        
        let addButton2 = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector (navigateTo(_:)))
        self.navigationItem.leftBarButtonItem = addButton2

        // Initialize Consumer
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
        } else {
            print ("Location service is not enabled. Connect to the Illinois node")
            yourForwarder = defaultForwarder
            initConsumer()
        }
        
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(saveImage(_:)))
        longPressGesture.minimumPressDuration = 1.5
        longPressGesture.delegate = self
        self.tableView.addGestureRecognizer(longPressGesture)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 250
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationItem.title = "You are visiting " + (prefix?.getComponentByIndex(-1)?.toUri().replacingOccurrences(of: "/", with: ""))!
        
        if (isFirst == false) {
            if let prevForwarderIP = prevForwarder?.IP {
                if (yourForwarder?.IP != prevForwarderIP) {
                    initConsumer()
                }
            } else {
                initConsumer()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
         var newIndex = [String]()
         for (key, _) in objects {
             if key.contains((prefix?.toUri())!) {
             newIndex.insert(key, at: 0)
             }
         }
        
        self.index = newIndex
        self.tableView.reloadData()
    }
    
    func initConsumer() {
        prevForwarder = yourForwarder
        c = Consumer(prefix: prefix!, protocols: protocolTypes.IDR, forwarderIP: (yourForwarder?.IP)!, forwarderPort: 6363)
        setContextOptions(consumer: &c!)
    }
    
    func setContextOptions(consumer: inout Consumer) {
        consumer.setContextOption(optionName: Options.MUST_BE_FRESH_S, optionValue: true)
        consumer.setContextOption(optionName: Options.INTEREST_LIFETIME, optionValue: 200)
        consumer.setContextOption(optionName: Options.INTEREST_LEAVE_CNTX, optionValue: processLeavingInterest)
        consumer.setContextOption(optionName: Options.DATA_ENTER_CNTX, optionValue: processData)
        consumer.setContextOption(optionName: Options.CONTENT_RETRIEVED, optionValue: processPayload)
        consumer.setContextOption(optionName: Options.ALL_LIST_CONSUMED, optionValue: processAllListConsumed)
        consumer.setContextOption(optionName: Options.ON_OPEN, optionValue: onOpen)
        
        consumer.setContextOption(optionName: Options.EXCLUDE_S, optionValue: index) // temporary
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        isFirst = false
        switch status {
        case .authorizedWhenInUse:
            if let currentLocation = locationManager.location {
                print ("Location service is enabled. Connect to the nearest node")
                yourForwarder = findNearestForwarder(currentLocation: currentLocation)
                initConsumer()
            } else {
                print ("Location service is enabled but current location can't be found. Connect to the Illinois node")
                yourForwarder = defaultForwarder
                initConsumer()
            }
            break
        default:
            print ("Location service is enabled but not authorized. Connect to the Illinois node")
            yourForwarder = defaultForwarder
            initConsumer()
            break
        }
    }
    
    func insertNewObject(_ sender: Any) {
        c?.consume()
        receivedData = 0
        downloadAlert()
    }
    
    func navigateTo(_ sender: Any) {
        
        if let targetCell = lastSelectedCell {
            let prefixString = index[targetCell].replacingOccurrences(of: appPrefix, with: "")
            var prefixComponents = prefixString.components(separatedBy: "/")
            prefixComponents = prefixComponents.filter { $0 != "" }
            prefixComponents.removeLast()
            
            ActionSheetStringPicker.show(withTitle: "Choose prefix", rows: prefixComponents, initialSelection: 1, doneBlock: {
                picker, value, index in
            
                let prefix = Name(url: appPrefix)
                
                for i in 0...value {
                    prefix?.appendComponent(prefixComponents[i])
                }
                
                self.launchViewController(prefix: prefix!)
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        }
    }
    
    func launchViewController(prefix: Name) {
        let viewController : PictureViewController = (UIStoryboard(name: "Main", bundle: nil) .instantiateViewController(withIdentifier: ViewControllerIdentifiers.PictureViewController) as? PictureViewController)!
        viewController.prefix = prefix
        
        var newIndex = [String]()
        for (key, value) in objects {
            if key.contains((prefix.toUri())) {
                newIndex.insert(key, at: 0)
            }
        }
        
        viewController.index = newIndex
        
        // Flip animation
        let transition = CATransition()
        transition.duration = 1
        transition.type = kCATransitionFade
        transition.subtype = kCATransitionFromLeft
        
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func saveImage(_ longPressGestureRecognizer: UILongPressGestureRecognizer) {
        if longPressGestureRecognizer.state == UIGestureRecognizerState.began {
            let touchPoint = longPressGestureRecognizer.location(in: self.view)
            if let indexPath = tableView.indexPathForRow(at: touchPoint) {
                lastSelectedCell = indexPath.row
                let image = objects[index[lastSelectedCell!]]! as UIImage
                
                let alertController = UIAlertController(title: "Save this image?", message: "Click OK to save the image", preferredStyle: .alert)
                
                let CancelAction = UIAlertAction(title: "Cancel", style: .default) { action in return }
                alertController.addAction(CancelAction)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { action in
                    UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
                }
                alertController.addAction(OKAction)
                
                present(alertController, animated: true)
            }
        }
    }
    
    func downloadAlert() {
        downloadAlertController = UIAlertController(title: "Please, Wait", message: "Image Loading...", preferredStyle: .alert)
        let CancelAction = UIAlertAction(title: "Cancel", style: .default) { action in return }
        downloadAlertController?.addAction(CancelAction)
        
        progressDownload = UIProgressView(progressViewStyle: .default)
        progressDownload?.frame = CGRect(x: 10, y: 70, width: 250, height: 0)
        downloadAlertController!.view.addSubview(progressDownload!)
        present(downloadAlertController!, animated: true)
    }
    
    func updateProgress(progress: Float){
        if (progress > 1.0) {
            progressDownload?.setProgress(1.0, animated: true)
        } else {
            progressDownload?.setProgress(progress, animated: true)
        }
        
    }

    // MARK: - Table View
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return index.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomCell
        let imageName = index[indexPath.row]
        let object = objects[imageName]
        cell.setCell(imageName: imageName, image: object!)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        lastSelectedCell = indexPath.row
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "The selected image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    // Consumer Callback functions
    
    func processLeavingInterest(consumer: Consumer, interest: SwiftNDNTouch.Interest) -> Void {
        print ("LEAVES "+interest.name.toUri())
    }
    
    func processData(consumer: Consumer, data: SwiftNDNTouch.Data) -> Void {
        print ("DATA IN CNTX")
        totalData = (data.getFinalBlockID()?.toSegment())! + 1
        receivedData = receivedData! + 1
        let progress = Float(self.receivedData!) / Float(self.totalData!)
        updateProgress(progress: progress)
    }
    
    func processPayload(consumer: Consumer, content: [UInt8]) -> Void {
        print ("IMAGE LOADING...")
        
        var imagePrefix = Name()
        consumer.getContextOption(optionName: Options.PREFIX, optionValue: &imagePrefix)
        
        var imageSuffix = Name()
        consumer.getContextOption(optionName: Options.SUFFIX, optionValue: &imageSuffix)
        
        let data = NSData(bytes: content, length: content.count)
        let image = UIImage(data: data as Foundation.Data)
        let imageName = imagePrefix.toUri()+imageSuffix.toUri()
        
        objects[imageName] = image
        index.insert(imageName, at: 0)
        
        let indexPath = IndexPath(row: 0, section: 0)
        
        downloadAlertController!.dismiss(animated: true, completion: { action in return })
        self.tableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    func processAllListConsumed(consumer: Consumer, prefix: Name) {
        downloadAlertController!.dismiss(animated: true, completion: { action in return })
        let alertController = UIAlertController(title: "No More Images", message: "Please travel another place", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default))
        present(alertController, animated: true)
    }
    
    func onOpen() {
        // if no image is on the storyboard, just download one
        insertNewObject(self)
    }

}
