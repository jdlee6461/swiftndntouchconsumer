//
//  CustomCell.swift
//  SwiftNDNTouchConsumer
//
//  Created by Jongdeog Lee on 12/22/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import Foundation
import UIKit

class CustomCell: UITableViewCell {
    
    @IBOutlet weak var imageName: UILabel!
    @IBOutlet weak var imageData: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setCell(imageName: String, image: UIImage) {
        self.imageName?.text = imageName.replacingOccurrences(of: appPrefix, with: "")
        self.imageData?.image = image
    }

    
//    func setCell(imageContents: imageContents) {
//        self.imageName?.text = imageContents.imageName
//        self.imageData?.image = imageContents.image
//    }
    
}
