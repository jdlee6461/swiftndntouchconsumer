//
//  Configuration.swift
//  SwiftNDNTouchConsumer
//
//  Created by Jongdeog Lee on 1/19/17.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
import SwiftNDNTouchConsumer

/* global variables */
var objects: [String:UIImage] = [:]  // global storage for received images
var yourForwarder: Forwarder?   // the forwarder that is currently connected to your app
var landmarkPins = [MapPin]()   // a set of pins in the map view
var countryPins = [MapPin]()
var continentPins = [MapPin]()
var forwarders = [Forwarder]()  // a set of forwarders
var c: Consumer?     // the consumer that requests images

/* view controller settings */
public struct ViewControllerIdentifiers {
    static let PictureViewController = "pictureViewController"
    static let MapViewController = "mapViewController"
    static let SettingViewController = "settingViewController"
}


/* some data structures & data */
public struct Forwarder {
    var name: String
    var shortName: String
    var IP: String
    var coordinate: CLLocation
}

public let defaultForwarder = forwarders[5]     // Illinois node
//public let appPrefix = "/ndn/edu/illinois/%40GUEST/jdlee6461%40gmail.com/VisualTourism"
public let appPrefix = "/ndn/edu/illinois/jlee700/VisualTourism"
public let rootPrefix = appPrefix + "/World"

func getAllForwarderNames() -> [String] {
    var lists = [String]()
    
    for i in 0..<forwarders.count {
        lists.append(forwarders[i].name)
    }
    return lists
}

func findNearestForwarder(currentLocation: CLLocation) -> Forwarder {
    
    var minDistance: CLLocationDistance = Double.greatestFiniteMagnitude
    var minIndex = -1
    
    for i in 0..<forwarders.count {
        let currentDistance = currentLocation.distance(from: forwarders[i].coordinate)
        if (minDistance > currentDistance) {
            minDistance = currentDistance
            minIndex = i
        }
    }
    
    print ("Connected to "+forwarders[minIndex].IP)
    return forwarders[minIndex]
}

// Forwarder Lists
