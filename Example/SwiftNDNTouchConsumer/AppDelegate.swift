//
//  AppDelegate.swift
//  SwiftNDNTouchConsumer
//
//  Created by Jongdeog Lee on 12/01/2016.
//  Copyright (c) 2016 Jongdeog Lee. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        appInit()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func appInit() {
        // Init MapPins
        if let landmarkListsJsonFile = Bundle.main.url(forResource: "landmarkLists", withExtension: "json") {
            do {
                let landmarkListsStr = try String(contentsOf: landmarkListsJsonFile, encoding: String.Encoding.utf8)
                // print(text)
                if let landmarkListsData = landmarkListsStr.data(using: .utf8, allowLossyConversion: false) {
                    let landmarkListsJson = JSON(data: landmarkListsData)
                    
                    for (_, landmark) in landmarkListsJson {
                        landmarkPins.append(MapPin(title: landmark["Title"].string!, locationName: landmark["Category"].string!, fullName: landmark["Name"].string!, coordinate: CLLocationCoordinate2D(latitude: landmark["Latitude"].double!, longitude: landmark["Longitude"].double!)))
                    }
                }
            } catch {
                print ("ERROR: Could not load the landmark lists content")
            }
            
        } else {
            print ("ERROR: Could not find the landmark lists file")
        }
        
        if let countryListsJsonFile = Bundle.main.url(forResource: "countryLists", withExtension: "json") {
            do {
                let countryListsStr = try String(contentsOf: countryListsJsonFile, encoding: String.Encoding.utf8)
                // print(text)
                if let countryListsData = countryListsStr.data(using: .utf8, allowLossyConversion: false) {
                    let countryListsJson = JSON(data: countryListsData)
                    
                    for (_, country) in countryListsJson {
                        countryPins.append(MapPin(title: country["Title"].string!, locationName: country["Category"].string!, fullName: country["Name"].string!, coordinate: CLLocationCoordinate2D(latitude: country["Latitude"].double!, longitude: country["Longitude"].double!)))
                    }
                }
            } catch {
                print ("ERROR: Could not load the landmark lists content")
            }
            
        } else {
            print ("ERROR: Could not find the landmark lists file")
        }
        
        if let continentListsJsonFile = Bundle.main.url(forResource: "continentLists", withExtension: "json") {
            do {
                let continentListsStr = try String(contentsOf: continentListsJsonFile, encoding: String.Encoding.utf8)
                // print(text)
                if let continentListsData = continentListsStr.data(using: .utf8, allowLossyConversion: false) {
                    let continentListsJson = JSON(data: continentListsData)
                    
                    for (_, continent) in continentListsJson {
                        continentPins.append(MapPin(title: continent["Title"].string!, locationName: continent["Category"].string!, fullName: continent["Name"].string!, coordinate: CLLocationCoordinate2D(latitude: continent["Latitude"].double!, longitude: continent["Longitude"].double!)))
                    }
                }
            } catch {
                print ("ERROR: Could not load the landmark lists content")
            }
            
        } else {
            print ("ERROR: Could not find the landmark lists file")
        }

        // Init forwarders
        if let forwarderListsJsonFile = Bundle.main.url(forResource: "forwarderLists", withExtension: "json") {
            do {
                let forwarderListsStr = try String(contentsOf: forwarderListsJsonFile, encoding: String.Encoding.utf8)
                if let forwarderListsData = forwarderListsStr.data(using: .utf8, allowLossyConversion: false) {
                    let forwarderListsJson = JSON(data: forwarderListsData)
                    
                    for (_, forwarder) in forwarderListsJson {
                        forwarders.append(Forwarder(name: forwarder["name"].string!, shortName: forwarder["shortname"].string!, IP: forwarder["site"].string!, coordinate: CLLocation(latitude: forwarder["position"][0].double!, longitude: forwarder["position"][1].double!)))
                    }
                }
            } catch {
                print ("ERROR: Could not load the forwarder lists content")
            }
            
        } else {
            print ("ERROR: Could not find the forwarder lists file")
        }
    }

}

