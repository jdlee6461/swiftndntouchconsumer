//
//  MapPin.swift
//  SwiftNDNTouchConsumer
//
//  Created by Jongdeog Lee on 1/26/17.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import Foundation
import MapKit

public class MapPin : NSObject, MKAnnotation {
    public let title: String?
    let locationName: String
    let fullName: String
    public let coordinate: CLLocationCoordinate2D
    
    init(title: String, locationName: String, fullName: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.fullName = fullName
        self.coordinate = coordinate
        
        super.init()
    }
    
    public var subtitle: String? {
        return locationName
    }
}

protocol MapViewDelegate : MKMapViewDelegate {
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?
}
