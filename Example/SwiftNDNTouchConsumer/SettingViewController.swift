//
//  SettingViewController.swift
//  SwiftNDNTouchConsumer
//
//  Created by Jongdeog Lee on 1/25/17.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import UIKit
import SwiftNDNTouchConsumer
import ActionSheetPicker_3_0

class SettingViewController: UITableViewController, UITextFieldDelegate {
    
    @IBOutlet weak var forwarderName: UIButton!
    @IBOutlet weak var nfdConnection: UISegmentedControl!
    @IBOutlet weak var retrievalProtocol: UISegmentedControl!
    @IBOutlet weak var imageQuality: UISegmentedControl!
    @IBOutlet weak var appPrefixName: UITextField!
    @IBOutlet weak var rootNodeName: UITextField!
    
    @IBAction func decideForwarder(_ sender: UIButton) {
        
        ActionSheetStringPicker.show(withTitle: "Choose forwarder", rows: getAllForwarderNames(), initialSelection: 0, doneBlock: {
            picker, value, index in
                self.forwarderName.setTitle(index as! String?, for: .normal)
                yourForwarder = forwarders[value]
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        appPrefixName.delegate = self
        rootNodeName.delegate = self
        
        appPrefixName.text = appPrefix
        rootNodeName.text = rootPrefix
        
        appPrefixName.isUserInteractionEnabled = false
        rootNodeName.isUserInteractionEnabled = false
        
        forwarderName.setTitle(yourForwarder?.name, for: .normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
