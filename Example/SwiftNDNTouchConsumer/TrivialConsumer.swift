//
//  TrivialConsumer.swift
//  Test
//
//  Created by Jongdeog Lee on 11/10/16.
//  Copyright © 2016 Jongdeog Lee. All rights reserved.
//

import Foundation
import SwiftNDNTouch

public protocol TrivialConsumerDelegate: class {
    func onOpen()
    func onClose()
    func onData(i: Interest, d: SwiftNDNTouch.Data)
    func onError(reason: String)
}

public class TrivialConsumer: FaceDelegate {
    
    weak var delegate: TrivialConsumerDelegate!
    
    private var face: Face!
    private var prefixString: String!
    private var prefixtName: Name!
    
    public init(delegate: TrivialConsumerDelegate, prefix: String, forwarderIP: String, forwarderPort: UInt16) {
        
        self.delegate = delegate
        prefixString = prefix
        face = Face(delegate: self, host: forwarderIP, port: forwarderPort)
        face.open()
    }
    
    deinit {
        close()
    }
    
    public func close() {
        face.close()
    }
    
    public func get(postfix: String) {
        print("Info: TrivialConsumer is expressing Interest: " + prefixString + postfix)
        let interest = Interest()
        
        interest.name = Name(url: prefixString+postfix)!
        interest.setInterestLifetime(1000)
        interest.setMustBeFresh()
        self.face.expressInterest(interest, onData: { [unowned self] in self.delegate.onData(i: $0, d: $1) }, onTimeout: { [unowned self] in self.onTimeout(i0: $0) })
    }
    
    private func onTimeout(i0: Interest) {
        print("Error: TrivialConsumer - timeout for " + i0.name.toUri())
    }
    
    // Face Delegate Functions
    public func onOpen() {
        delegate.onOpen()
    }
    
    public func onClose() {
        delegate.onClose()
    }
    
    public func onError(_ reason: String) {
        print("ERROR: TrivialConsumer - " + reason)
    }
    
}
