//
//  MapViewController.swift
//  SwiftNDNTouchConsumer
//
//  Created by Jongdeog Lee on 1/25/17.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import UIKit
import MapKit
import SwiftNDNTouchConsumer
import ActionSheetPicker_3_0
import CoreLocation

class MapViewController: UIViewController, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    var prevZoomFactor: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        mapView.delegate = self
        prevZoomFactor = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func launchViewController(prefix: Name) {
        let viewController : PictureViewController = (UIStoryboard(name: "Main", bundle: nil) .instantiateViewController(withIdentifier: ViewControllerIdentifiers.PictureViewController) as? PictureViewController)!
        viewController.prefix = prefix
        
        var newIndex = [String]()
        for (key, _) in objects {
            if key.contains((prefix.toUri())) {
                newIndex.insert(key, at: 0)
            }
        }
        
        viewController.index = newIndex
        
        // Flip animation
        let transition = CATransition()
        transition.duration = 1
        transition.type = kCATransitionFade
        transition.subtype = kCATransitionFromLeft
        
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    // MapView delegates
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? MapPin {
            let identifier = "Pin"
            var view: MKPinAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                as? MKPinAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            }
            return view
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let pin = view.annotation as? MapPin
        print("info button pressed for \(pin?.title)")
        
        // Segue into the tweet-table view while also pushing the new tweet-table view onto the nagivation stack
        if let prefix = Name(url: rootPrefix+(pin?.fullName)!) {
            launchViewController(prefix: prefix)
        } else {
            print("ERROR: check pin's title format")
        }
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let zoomWidth = mapView.visibleMapRect.size.width
        let zoomFactor = Int(log2(zoomWidth)) - 9
        
        if (prevZoomFactor != zoomFactor) {
            print("...REGION DID CHANGE: ZOOM FACTOR \(zoomFactor)")
            prevZoomFactor = zoomFactor
            
            let allAnnotations = self.mapView.annotations
            self.mapView.removeAnnotations(allAnnotations)
            
            if (zoomFactor >= 17) {
                mapView.addAnnotations(continentPins)
                self.navigationItem.title = "Continent View"
            } else if (zoomFactor == 16) {
                mapView.addAnnotations(countryPins)
                self.navigationItem.title = "Country View"
            } else if (zoomFactor <= 15) {
                mapView.addAnnotations(landmarkPins)
                self.navigationItem.title = "Landmark View"
            }
        }
    }
}


