//
//  imageContents.swift
//  SwiftNDNTouchConsumer
//
//  Created by Jongdeog Lee on 12/28/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import Foundation
import UIKit

class imageContents {
    var imageName = "/ndn/images"
    var image = UIImage()
    
    init(imageName: String, image: UIImage) {
        self.imageName = imageName
        self.image = image
    }
}
