//
//  ContextDefaultValues.swift
//  SwiftNDNTouchConsumer
//
//  Created by Jongdeog Lee on 11/15/16.
//  Copyright © 2016 Jongdeog Lee. All rights reserved.
//

import Foundation
import SwiftNDNTouch

/* this file contains various default values
 * numbers here are not unique
 */

let EMPTY_CALLBACK = 0

public struct protocolTypes {
    public static let SDR = 0
    public static let UDR = 1
    public static let RDR = 2
    public static let IDR = 3
}

public struct fowardingStrategies {
    public static let BEST_ROUTE = Name(url: "ndn:/localhost/nfd/strategy/best-route")
    public static let BROADCAST = Name(url: "ndn:/localhost/nfd/strategy/broadcast")
    public static let CLIENT_CONTROL = Name(url: "ndn:/localhost/nfd/strategy/client-control")
}

public struct defaultValues {
    public static let INTEREST_LIFETIME = 200  // milliseconds
    public static let DATA_FRESHNESS = 100000  // milliseconds ~= 100 seconds
    public static let DATA_PACKET_SIZE = 2048  // bytes
    public static let INTEREST_SCOPE = 2
    public static let MIN_SUFFIX_COMP = -1
    public static let MAX_SUFFIX_COMP = -1
    public static let PRODUCER_RCV_BUFFER_SIZE = 1000 // of Interests
    public static let PRODUCER_SND_BUFFER_SIZE = 1000 // of Data
    public static let KEY_LOCATOR_SIZE = 256          // of bytes
    public static let SAFETY_OFFSET = 10              // of bytes
    public static let MIN_WINDOW_SIZE = 1             // of Interests
    public static let MAX_WINDOW_SIZE = 1            // of Interests
    public static let DIGEST_SIZE = 32                // of bytes
    public static let FAST_RETX_CONDITION = 3         // of out-of-order segments
}

public struct maxAllowedValues {
    public static let CONSUMER_MIN_RETRANSMISSIONS = 0
    public static let CONSUMER_MAX_RETRANSMISSIONS = 32
    public static let DEFAULT_MAX_EXCLUDED_DIGESTS = 5
    public static let MAX_DATA_PACKET_SIZE = 8096
}

public struct optionValues {
    public static let FOUND = 0
    public static let NOT_FOUND = 1
    public static let VALUE_SET = 2
    public static let VALUE_NOT_SET = 3
    public static let DEFAULT_VALUE = 666 // some rare number
}

public struct miscValues {
    public static let PRODUCER_OPERATION_FAILED = 10
    public static let CONSUMER_READY = 0
    public static let CONSUMER_BUSY = 1
    public static let CONSUMER_FAILED = 2
    
    public static let REGISTRATION_NOT_ATTEMPTED = 0
    public static let REGISTRATION_SUCCESS = 1
    public static let REGISTRATION_FAILURE = 2
    public static let REGISTRATION_IN_PROGRESS = 3
    
    public static let LEFTMOST_CHILD = 0
    public static let RIGHTMOST_CHILD = 1
    
    public static let SHA_256 = 1
    public static let RSA_256 = 2
}

public struct NACK {
    public static let DATA_TYPE:UInt64 = UInt64(Tlv.ContentTypeValue.ContentType_Nack)
//    public static let DELAY = 1
//    public static let INTEREST_NOT_VERIFIED = 2
}

public struct InfoMax {
    // public static let DEFAULT_LIST_SIZE = 10     // only for producer
    public static let INTEREST_TAG = "InfoMax"
    public static let META_INTEREST_TAG = "MetaInfo"
    // public static let DEFAULT_UPDATE_INTERVAL = 5000   // 5 seconds, only for producer
    
    struct Prioritizer {
        public static let NONE = 0
        public static let SIMPLE = 1
        public static let MERGE = 2
    }
}

public let CONTENT_DATA_TYPE: UInt64 = UInt64(Tlv.ContentTypeValue.ContentType_Blob)

//public struct emtpyCallbacks {
//    public static let consumerInterest: ConsumerInterestCallback = {_,_ in}
//    public static let consumercontent: ConsumerContentCallback = {_,_ in}
//    public static let consumerData: ConsumerDataCallback = {_,_ in}
//    public static let consumerDataVerification: ConsumerDataVerificationCallback = {_,_ in return false}
//}
