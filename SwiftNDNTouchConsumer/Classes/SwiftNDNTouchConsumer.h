//
//  SwiftNDNTouchConsumer.h
//  SwiftNDNTouchConsumer
//
//  Created by Jongdeog Lee on 11/15/16.
//  Copyright © 2016 Jongdeog Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SwiftNDNTouchConsumer.
FOUNDATION_EXPORT double SwiftNDNTouchConsumerVersionNumber;

//! Project version string for SwiftNDNTouchConsumer.
FOUNDATION_EXPORT const unsigned char SwiftNDNTouchConsumerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SwiftNDNTouchConsumer/PublicHeader.h>

@import SwiftNDNTouch;
