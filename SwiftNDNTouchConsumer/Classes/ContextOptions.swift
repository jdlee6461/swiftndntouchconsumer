//
//  ContextOptions.swift
//  SwiftNDNTouchConsumer
//
//  Created by Jongdeog Lee on 11/15/16.
//  Copyright © 2016 Jongdeog Lee. All rights reserved.
//

import Foundation

/* This file contains recognized values for the first parameter of get/setContextOption
 * every number is unique
 */

public struct Options {
    /* This file contains recognized values for the first parameter of get/setContextOption
     *  every number is unique
     */
    
    public static let RCV_BUF_SIZE            = 1   // int
    public static let SND_BUF_SIZE            = 2   // int
    public static let PREFIX                  = 3   // Name
    public static let SUFFIX                  = 4   // Name
    public static let REMOTE_REPO_PREFIX      = 5   // Name
    public static let LOCAL_REPO              = 6   // bool
    public static let INTEREST_RETX           = 7   // int
    public static let DATA_PKT_SIZE           = 8   // int
    public static let INTEREST_LIFETIME       = 9   // int
    public static let FORWARDING_STRATEGY     = 10  // int
    public static let DATA_FRESHNESS          = 11  // int
    public static let REGISTRATION_STATUS     = 12  // int
    public static let KEY_LOCATOR             = 13  // KeyLocator
    public static let SIGNATURE_TYPE          = 14  // int
    public static let MIN_WINDOW_SIZE         = 15  // int
    public static let MAX_WINDOW_SIZE         = 16  // int
    public static let CURRENT_WINDOW_SIZE     = 17  // int
    public static let MAX_EXCLUDED_DIGESTS    = 18  // int
    public static let ASYNC_MODE              = 19  // bool
    public static let FAST_SIGNING            = 20  // bool
    public static let FACE                    = 21  // Face
    public static let RUNNING                 = 22  // bool
    public static let INFOMAX                 = 23  // bool
    public static let INFOMAX_ROOT            = 24  // TreeNode
    public static let INFOMAX_PRIORITY        = 25  // int
    public static let INFOMAX_UPDATE_INTERVAL = 26  // int (milliseconds)
    
    // selectors
    public static let MIN_SUFFIX_COMP_S   = 101 // int
    public static let MAX_SUFFIX_COMP_S   = 102 // int
    public static let EXCLUDE_S           = 103 // Exclude
    public static let MUST_BE_FRESH_S     = 104 // bool
    public static let LEFTMOST_CHILD_S    = 105 // int
    public static let RIGHTMOST_CHILD_S   = 106 // int
    public static let KEYLOCATOR_S        = 107 // KeyLocator
    
    // consumer context events
    public static let INTEREST_LEAVE_CNTX      = 201 // InterestCallback
    public static let INTEREST_RETRANSMIT      = 202 // InterestCallback
    public static let INTEREST_EXPIRED         = 203 // ConstInterestCallback
    public static let INTEREST_SATISFIED       = 204 // ConstInterestCallback
    
    public static let DATA_ENTER_CNTX          = 211 // DataCallback
    public static let NACK_ENTER_CNTX          = 212 // ConstNackCallback
    public static let MANIFEST_ENTER_CNTX      = 213 // ConstManifestCallback
    public static let DATA_TO_VERIFY           = 214 // DataVerificationCallback
    public static let CONTENT_RETRIEVED        = 215 // ContentCallback
    
    public static let ALL_LIST_CONSUMED        = 216 // 
    
    public static let ON_OPEN                  = 217
    public static let ON_CLOSE                 = 218
    public static let ON_ERROR                 = 219
    
    
    // producer context events
    public static let INTEREST_ENTER_CNTX      = 301
    public static let INTEREST_DROP_RCV_BUF    = 302
    public static let INTEREST_PASS_RCV_BUF    = 303
    public static let CACHE_HIT                = 306
    public static let CACHE_MISS               = 308
    public static let NEW_DATA_SEGMENT         = 309 // DataCallback
    public static let DATA_TO_SECURE           = 313 // DataCallback
    public static let DATA_IN_SND_BUF          = 310 // DataCallback
    public static let DATA_LEAVE_CNTX          = 311 // ConstDataCallback
    public static let DATA_EVICT_SND_BUF       = 312 // ConstDataCallback
}
