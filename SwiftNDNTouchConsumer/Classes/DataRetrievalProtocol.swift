//
//  DataRetrievalProtocol.swift
//  SwiftNDNTouchConsumer
//
//  Created by Jongdeog Lee on 11/15/16.
//  Copyright © 2016 Jongdeog Lee. All rights reserved.
//

import Foundation
import SwiftNDNTouch

class DataRetrievalProtocol: FaceDelegate
{
    var m_consumer:Consumer?
    var m_face:Face?
    var m_isRunning:Bool
        
    init() {
        self.m_isRunning = false
    }
    
    init(consumer: Consumer) {
        self.m_consumer = consumer
        self.m_isRunning = false
        self.m_face = Face(delegate: self)
    }
    
    func isRunning() -> Bool {
        return m_isRunning
    }
    
    func updateFace() -> Void {
        m_consumer?.getContextOption(optionName: Options.FACE, optionValue: &m_face!)
    }
    
    func start() -> Void {
        fatalError("Must Override")
    }
    
    func stop() -> Void {
        fatalError("Must Override")
    }
    
    func onOpen() {
        print ("Face open")
    }
    
    func onClose() {
        print ("Face close")
    }
    
    func onError(_ reason: String) {
        print ("Face error")
    }
}
