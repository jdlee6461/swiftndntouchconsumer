//
//  InfomaxDataRetrieval.swift
//  Pods
//
//  Created by Jongdeog Lee on 12/21/16.
//
//

import Foundation
import SwiftNDNTouch

class InfomaxDataRetrieval: DataRetrievalProtocol {
    
    /* InfoMax variables */
    var m_infomaxList = [String]()
    var m_requestVersion: UInt64?
    var m_requestListNum: UInt64?
    var m_maxListNum: UInt64?
    var m_isInit: Bool?
    
    /* To preserve original user-defined callback functions */
    var tempProcessLeavingInterest:ConsumerInterestCallback = {_,_ in}
    var tempProcessData:ConsumerDataCallback = {_,_ in}
    var tempProcessPayload:ConsumerContentCallback = {_,_ in}
    
    public override init(consumer: Consumer) {
        super.init(consumer: consumer)
        m_requestListNum = 1
        m_requestVersion = 1
        m_maxListNum = 0
        m_isInit = true
    }
    
    public override func start() {
        m_isRunning = true
        
        if (m_isInit! == true) {
            sendInitInfoMaxInterest()
        } else {
            if (m_infomaxList.isEmpty == true) {
                sendInfoMaxInterest()
            } else {
                sendInterest()
            }
        }
    }
    
    public override func stop() {
        m_infomaxList.removeAll()
        m_isRunning = false
    }
    
    func sendInitInfoMaxInterest() {
        /* Request version number and the total number of lists (/prefix/InfoMax/MetaInfo) */

        m_isInit = false
        
        m_consumer?.getContextOption(optionName: Options.INTEREST_LEAVE_CNTX, optionValue: &tempProcessLeavingInterest)
        m_consumer?.getContextOption(optionName: Options.DATA_ENTER_CNTX, optionValue: &tempProcessData)
        m_consumer?.getContextOption(optionName: Options.CONTENT_RETRIEVED, optionValue: &tempProcessPayload)
        
        m_consumer?.setContextOption(optionName: Options.MUST_BE_FRESH_S, optionValue: true)
        m_consumer?.setContextOption(optionName: Options.INTEREST_LEAVE_CNTX, optionValue: processLeavingInfoMaxInitInterest)
        m_consumer?.setContextOption(optionName: Options.DATA_ENTER_CNTX, optionValue: processInfoMaxInitData)
        m_consumer?.setContextOption(optionName: Options.CONTENT_RETRIEVED, optionValue: processInfoMaxInitPayload)
        
        var infomaxInitSuffix = Name()
        infomaxInitSuffix.appendComponent(InfoMax.INTEREST_TAG)
        infomaxInitSuffix.appendComponent(InfoMax.META_INTEREST_TAG)
        infomaxInitSuffix.appendNumber(1)
        
        m_consumer?.setContextOption(optionName: Options.SUFFIX, optionValue: infomaxInitSuffix)
        
        var m_rdr = ReliableDataRetrieval(consumer: m_consumer!)
        m_rdr.start()
    }
    
    func sendInfoMaxInterest() -> Bool {
        /* Request InfoMax lists (/prefix/InfoMax/version#/list#) */

        if(m_requestListNum! > m_maxListNum!) {
            var onAllListsConsumed: ConsumerFullListCallback = {_,_ in }
            if (m_consumer?.getContextOption(optionName: Options.ALL_LIST_CONSUMED, optionValue: &onAllListsConsumed) == optionValues.FOUND) {
                onAllListsConsumed(m_consumer!, (m_consumer?.m_prefix)!)
            } else {
                processAllListConsumed(consumer: m_consumer!, prefix: (m_consumer?.m_prefix)!)
            }
            return false
        }

        var infomaxSuffix = Name()
        infomaxSuffix.appendComponent(InfoMax.INTEREST_TAG)
        infomaxSuffix.appendNumber(m_requestVersion!)
        infomaxSuffix.appendNumber(m_requestListNum!)
        m_requestListNum = m_requestListNum! + 1

        m_consumer?.setContextOption(optionName: Options.MUST_BE_FRESH_S, optionValue: true)
        m_consumer?.setContextOption(optionName: Options.INTEREST_LEAVE_CNTX, optionValue: processLeavingInfoMaxInterest)
        m_consumer?.setContextOption(optionName: Options.DATA_ENTER_CNTX, optionValue: processInfoMaxData)
        m_consumer?.setContextOption(optionName: Options.CONTENT_RETRIEVED, optionValue: processInfoMaxPayload)

        m_consumer?.setContextOption(optionName: Options.RUNNING, optionValue: false)
        m_consumer?.setContextOption(optionName: Options.SUFFIX, optionValue: infomaxSuffix)
        
        var m_rdr = ReliableDataRetrieval(consumer: m_consumer!)
        m_rdr.start()
        
        return true
    }
    
    func sendInterest() {
        /* Request actual data (/prefix/suffix) */
        var suffix = ""
        
        if (m_infomaxList.isEmpty == false) {
            suffix = m_infomaxList.popLast()!
        } else {
            sendInfoMaxInterest()
        }
        
        let fullName = (m_consumer?.m_prefix?.toUri())! + suffix
        
        if (suffix.isEmpty == true) {
            print ("This interest name is empty")
            return
        }
        
        if let excludes = m_consumer?.m_excludes {
            if (excludes.contains(fullName) == true) {
                print ("This interest name is excluded")
                sendInterest()
                return
            }
        }
        
        var m_udr = UnreliableDataRetrieval(consumer: m_consumer!)
        
        m_consumer?.setContextOption(optionName: Options.INTEREST_LEAVE_CNTX, optionValue: tempProcessLeavingInterest)
        m_consumer?.setContextOption(optionName: Options.DATA_ENTER_CNTX, optionValue: tempProcessData)
        m_consumer?.setContextOption(optionName: Options.CONTENT_RETRIEVED, optionValue: tempProcessPayload)
        
        m_consumer?.setContextOption(optionName: Options.RUNNING, optionValue: false)
        m_consumer?.setContextOption(optionName: Options.SUFFIX, optionValue: Name(url: suffix)!)
        m_udr.start()        
    }
    
    func processInfoMaxInitPayload(consumer: Consumer, buffer: [UInt8]) {
        /* Fetching the latest version number and the total number of lists */
        let content = NSString(bytes: buffer, length: buffer.count, encoding: String.Encoding.utf8.rawValue) as! String
        print ("METAINFO IS BEING PROCESSED", content)
        var metaInfo = content.components(separatedBy: " ")
        
        m_requestVersion = UInt64(metaInfo[0])
        m_maxListNum = UInt64(metaInfo[1])
        
        sendInfoMaxInterest()
    }
    
    func processInfoMaxInitData(consumer: Consumer, data: SwiftNDNTouch.Data) {
        print ("METAINFO IN CNTX")
    }
    
    func processLeavingInfoMaxInitInterest(consumer: Consumer, interest: Interest) {
        print ("INFOMAX INIT INTEREST LEAVES")
        print (interest.name.toUri())
    }
    
    func processInfoMaxPayload(consumer: Consumer, buffer: [UInt8]) {
        /* Fetching the InfoMax list */
        let content = NSString(bytes: buffer, length: buffer.count, encoding: String.Encoding.utf8.rawValue)
        
        convertStringToList(names: content as! String)
        
        print ("InfoMax list is received")
        
        if (m_infomaxList.isEmpty == false) {
            sendInterest()
        } else {
            print ("InfoMAX list is empty")
        }
    }
    
    func processInfoMaxData(consumer: Consumer, data: SwiftNDNTouch.Data) {
        print ("LIST IN CNTX")
    }
    
    func processLeavingInfoMaxInterest(consumer: Consumer, interest: Interest) {
        print ("INFOMAX INTEREST LEAVES")
        print (interest.name.toUri())
    }
    
    func processAllListConsumed(consumer: Consumer, prefix: Name) {
        /* When there are no more data to retrieve */
        print("All data are fetched")
    }
    
    func convertStringToList(names: String) -> Void {
        m_infomaxList.removeAll()
        m_infomaxList = names.components(separatedBy: " ")
        m_infomaxList = m_infomaxList.filter { $0 != "" }
        m_infomaxList.reverse()
    }
}
