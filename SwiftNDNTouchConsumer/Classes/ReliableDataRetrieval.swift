//
//  ReliableDataRetrieval.swift
//  Pods
//
//  Created by Jongdeog Lee on 12/20/16.
//
//

import Foundation
import SwiftNDNTouch

class ReliableDataRetrieval: DataRetrievalProtocol {
    
    /* Reassembly variables */
    var m_isFinalBlockNumberDiscovered: Bool?
    var m_finalBlockNumber: UInt64?
    var m_contentBuffer = [UInt8]()
    
    /* Transmission variables */
    var m_currentWindowSize: Int?
    var m_interestsInFlight: Int?
    var m_segNumber: UInt64?
    
    /* Re-transmission variables */
    var m_interestRetransmissions: [UInt64: Int] = [:]
    
    /* buffers */
    var m_receiveBuffer: [UInt64: SwiftNDNTouch.Data] = [:]
    
    public override init(consumer: Consumer) {
        super.init(consumer: consumer)
        consumer.getContextOption(optionName: Options.FACE, optionValue: &m_face!)
    }
    
    public override func start() {
        m_isRunning = true
        m_isFinalBlockNumberDiscovered = false
        m_finalBlockNumber = UInt64.max
        m_segNumber = 0
        m_interestsInFlight = 0
        m_contentBuffer.removeAll()
        
        m_currentWindowSize = 0
        
        sendInterest()
    }
    
    public override func stop() {
        m_isRunning = false
        removeAllPendingInterests()
    }
    
    func sendInterest() -> Void {
        var prefix: Name = Name()
        m_consumer?.getContextOption(optionName: Options.PREFIX, optionValue: &prefix)
        
        var suffix: Name = Name()
        m_consumer?.getContextOption(optionName: Options.SUFFIX, optionValue: &suffix)
        
        if (suffix.toUri().isEmpty == false) {
            prefix = Name(url: prefix.toUri()+suffix.toUri())!
        }
        
        prefix.appendSegment(segmentNo: m_segNumber!)
        
        let interest = Interest()
        interest.name = prefix
        var interestLifetime = 0
        m_consumer?.getContextOption(optionName: Options.INTEREST_LIFETIME, optionValue: &interestLifetime)
        interest.setInterestLifetime(UInt64(interestLifetime)) // milliseconds
        
        var onInterestToLeaveContext: ConsumerInterestCallback = {_,_ in }
        
        m_consumer?.getContextOption(optionName: Options.INTEREST_LEAVE_CNTX, optionValue: &onInterestToLeaveContext)
        
        if (m_consumer?.getContextOption(optionName: Options.INTEREST_LEAVE_CNTX, optionValue: &onInterestToLeaveContext) == optionValues.FOUND) {
            onInterestToLeaveContext(m_consumer!, interest)
        }
        
        m_interestsInFlight = m_interestsInFlight!+1
        
        m_face?.expressInterest(interest, onData: onData, onTimeout: onTimeout)
        m_segNumber = m_segNumber! + 1
    }
    
    func onData(interest: Interest, data: SwiftNDNTouch.Data) -> Void {
        if (m_isRunning == false) {
            return
        }
        
        m_interestsInFlight = m_interestsInFlight! - 1
        
        var segment = interest.name.getComponentByIndex(-1)?.toSegment()
        
        /* Measuring RTT part is not included */
        
        var onDataEnteredContext: ConsumerDataCallback = {_,_ in }
        
        if (m_consumer?.getContextOption(optionName: Options.DATA_ENTER_CNTX, optionValue: &onDataEnteredContext) == optionValues.FOUND) {
            onDataEnteredContext(m_consumer!, data)
        }
        
        var onInterestSatisfied: ConsumerInterestCallback = {_,_ in }
        
        if (m_consumer?.getContextOption(optionName: Options.INTEREST_SATISFIED, optionValue: &onInterestSatisfied) == optionValues.FOUND) {
            onInterestSatisfied(m_consumer!, interest)
        }
        
        var onDataToVerify: ConsumerDataVerificationCallback = {_,_ in return false}
        var isDataSecure = false
        
        
        if (m_consumer?.getContextOption(optionName: Options.DATA_TO_VERIFY, optionValue: &onDataToVerify) == optionValues.FOUND) {
            if (onDataToVerify(m_consumer!, data) == true) {
                isDataSecure = true
            }
        } else {
            isDataSecure = true
        }
        
        if (data.content.type == Tlv.NDNType.Content) {
            onContentData(interest: interest, data: data)
        } else {
            print ("ERROR: The type of the received packet is not Content: ", data.content.type)
        }
        
        /* Currently, maxWindowSize is set to 1. Packet pipelining is not working properly */
        if (segment == 0) {
            m_currentWindowSize = 1        // or m_currentWindowSize = m_finalBlockNumber!
            
            var maxWindowSize = -1
            m_consumer?.getContextOption(optionName: Options.MAX_WINDOW_SIZE, optionValue: &maxWindowSize)
            
            if (m_currentWindowSize! > maxWindowSize) {
                m_currentWindowSize = maxWindowSize
            }
            
            while (m_interestsInFlight! < m_currentWindowSize!) {
                if (m_isFinalBlockNumberDiscovered == true) {
                    if (m_segNumber! <= m_finalBlockNumber!) {
                        sendInterest()
                    } else {
                        break
                    }
                } else {
                    sendInterest()
                }
            }
        } else {
            if (m_isRunning) {
                while (m_interestsInFlight! < m_currentWindowSize!) {
                    if (m_isFinalBlockNumberDiscovered! == true) {
                        if (m_segNumber! <= m_finalBlockNumber!) {
                            sendInterest()
                        } else {
                            break
                        }
                    } else {
                        sendInterest()
                    }
                }
            }
        }
    }
    
    func onContentData(interest: Interest, data: SwiftNDNTouch.Data) {
        var onDataToVerify: ConsumerDataVerificationCallback = {_,_ in return false}
        var isDataSecure = true
        
        // For now data is assumed to be verified
        
        if (isDataSecure) {
            var maxWindowSize = -1
            m_consumer?.getContextOption(optionName: Options.MAX_WINDOW_SIZE, optionValue: &maxWindowSize)
            if (m_currentWindowSize! < maxWindowSize) {
                m_currentWindowSize = m_currentWindowSize! + 1
                m_consumer?.setContextOption(optionName: Options.CURRENT_WINDOW_SIZE, optionValue: m_currentWindowSize!)
            }
            
            if (data.getFinalBlockID()?.isEmpty() == false) {
                m_isFinalBlockNumberDiscovered = true
                m_finalBlockNumber = data.getFinalBlockID()?.toSegment()
            }
            
            m_receiveBuffer[(data.name.getComponentByIndex(-1)?.toSegment())!] = data
            reassemble()
        }
    }
    
    func onTimeout(interest: Interest) -> Void {
        if(m_isRunning == false) {
            return
        }
        
        print ("ERROR: Trying to retransmit ", interest.name)
        
        m_interestsInFlight = m_interestsInFlight! - 1
        
        var onInterestExpired: ConsumerInterestCallback = {_,_ in}
        if (m_consumer?.getContextOption(optionName: Options.INTEREST_EXPIRED, optionValue: &onInterestExpired) == optionValues.FOUND) {
            onInterestExpired(m_consumer!, interest)
        }
        
        var segment = (interest.name.getComponentByIndex(-1)?.toSegment())!
        if (m_isFinalBlockNumberDiscovered == true) {
            if (segment > m_finalBlockNumber!) {
                return
            }
        }
        
        var minWindowSize = -1
        m_consumer?.getContextOption(optionName: Options.MIN_WINDOW_SIZE, optionValue: &minWindowSize)
        
        if (m_currentWindowSize! > minWindowSize) {
            m_currentWindowSize = m_currentWindowSize! / 2
            if (m_currentWindowSize == 0) {
                m_currentWindowSize = m_currentWindowSize! + 1
            }
            m_consumer?.setContextOption(optionName: Options.CURRENT_WINDOW_SIZE, optionValue: m_currentWindowSize!)
        }
        
        /* Implement the retransmission part here */
        
        var maxRetransmissions = 0
        m_consumer?.getContextOption(optionName: Options.INTEREST_RETX, optionValue: &maxRetransmissions)
        
        if let segRetransmission = m_interestRetransmissions[segment] {
            m_interestRetransmissions[segment] = m_interestRetransmissions[segment]! + 1
        } else {
            m_interestRetransmissions[segment] = 1
        }
        
        if m_interestRetransmissions[segment]! < maxRetransmissions {
            var retxInterest = Interest()
            retxInterest.name = interest.name
            
            var interestLifetime = defaultValues.INTEREST_LIFETIME
            m_consumer?.getContextOption(optionName: Options.INTEREST_LIFETIME, optionValue: interestLifetime)
            
            // Set Exclude after defining Exclude class
            
            var onInterestToRetransmitted: ConsumerInterestCallback = {_,_ in }
            
            if (m_consumer?.getContextOption(optionName: Options.INTEREST_RETRANSMIT, optionValue: &onInterestToRetransmitted) == optionValues.FOUND) {
                onInterestToRetransmitted(m_consumer!, interest)
            }
            
            var onInterestToLeaveContext: ConsumerInterestCallback = {_,_ in }
            
            if (m_consumer?.getContextOption(optionName: Options.INTEREST_LEAVE_CNTX, optionValue: &onInterestToLeaveContext) == optionValues.FOUND) {
                onInterestToLeaveContext(m_consumer!, interest)
            }
            
            // Because use could stop the context in one of the previous callbacks
            if (m_isRunning == false) {
                return
            }
            
            // retransmit
            m_interestsInFlight = m_interestsInFlight! + 1
            m_face?.expressInterest(interest, onData: onData, onTimeout: onTimeout)
            
        } else {
            m_isRunning = false
            reassemble()
        }
    }
    
    func reassemble() -> Void {
        if m_receiveBuffer.count == 0 {
            return
        }
        
        let lastIndex = UInt64(m_receiveBuffer.count) - 1
        if (m_finalBlockNumber! > lastIndex) {
            /* Need to wait */
        } else if (m_finalBlockNumber! < lastIndex) {
            /* Something wrong */
            print ("Error: there are more number of data than required")
        } else {
            for i in 0..<m_receiveBuffer.count {
                m_contentBuffer = m_contentBuffer + (m_receiveBuffer[UInt64(i)]?.getContent())!
            }
            var onPayload: ConsumerContentCallback = {_,_ in}
            if (m_consumer?.getContextOption(optionName: Options.CONTENT_RETRIEVED, optionValue: &onPayload) == optionValues.FOUND) {
                onPayload(m_consumer!, m_contentBuffer)
            }
            m_receiveBuffer.removeAll()
            m_contentBuffer.removeAll()
        }
    }
    
    func removeAllPendingInterests() {
        // Need to be implemented
        m_face?.close()
    }
}
