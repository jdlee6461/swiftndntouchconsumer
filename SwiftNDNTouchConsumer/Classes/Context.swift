//
//  Context.swift
//  SwiftNDNTouchConsumer
//
//  Created by Jongdeog Lee on 11/15/16.
//  Copyright © 2016 Jongdeog Lee. All rights reserved.
//

import Foundation
import SwiftNDNTouch

public typealias ConsumerInterestCallback = (Consumer, Interest) -> Void
public typealias ConsumerContentCallback = (Consumer, [UInt8]) -> Void
public typealias ConsumerDataCallback = (Consumer, SwiftNDNTouch.Data) -> Void
public typealias ConsumerDataVerificationCallback = (Consumer, SwiftNDNTouch.Data) -> Bool
public typealias ConsumerFullListCallback = (Consumer, Name) -> Void
public typealias ConsumerFaceCallback = () -> Void

/* Currently, there is only one type of context, Consumer. */
public class Context
{
    
    /* Setter functions */
    
    public func setContextOption(optionName: Int, optionValue: Int) -> Int {
        // function body
        return 0
    }
    
    public func setContextOption(optionName: Int, optionValue: Bool) -> Int {
        // function body
        return 0
    }
    
    public func setContextOption(optionName: Int, optionValue: Name) -> Int {
        // function body
        return 0
    }
    
    public func setContextOption(optionName: Int, optionValue: @escaping ConsumerDataCallback) -> Int {
        // function body
        return 0
    }
    
    public func setContextOption(optionName: Int, optionValue: @escaping ConsumerDataVerificationCallback) -> Int {
        // function body
        return 0
    }
    
    public func setContextOption(optionName: Int, optionValue: @escaping ConsumerInterestCallback) -> Int {
        // function body
        return 0
    }
    
    public func setContextOption(optionName: Int, optionValue: @escaping ConsumerContentCallback) -> Int {
        // function body
        return 0
    }
    
    public func setContextOption(optionName: Int, optionValue: @escaping ConsumerFullListCallback) -> Int {
        // function body
        return 0
    }
    
    public func setContextOption(optionName: Int, optionValue: @escaping ConsumerFaceCallback) -> Int {
        // function body
        return 0
    }
    
   
    /* Getter function */
    public func getContextOption(optionName: Int, optionValue: Int) -> Int {
        // function body
        return 0
    }
    
    public func getContextOption(optionName: Int, optionValue: inout Int) -> Int {
        // function body
        return 0
    }
    
    public func getContextOption(optionName: Int, optionValue: inout Bool) -> Int {
        // function body
        return 0
    }
    
    public func getContextOption(optionName: Int, optionValue: Name) -> Int {
        // function body
        return 0
    }
    
    public func getContextOption(optionName: Int, optionValue: inout Name) -> Int {
        // function body
        return 0
    }

    public func getContextOption(optionName: Int, optionValue: ConsumerDataVerificationCallback) -> Int {
        // function body
        return 0
    }
    
    public func getContextOption(optionName: Int, optionValue: inout ConsumerDataVerificationCallback) -> Int {
        // function body
        return 0
    }
    
    public func getContextOption(optionName: Int, optionValue: ConsumerDataCallback) -> Int {
        // function body
        return 0
    }
    
    public func getContextOption(optionName: Int, optionValue: inout ConsumerDataCallback) -> Int {
        // function body
        return 0
    }
    
    public func getContextOption(optionName: Int, optionValue: ConsumerInterestCallback) -> Int {
        // function body
        return 0
    }
    
    public func getContextOption(optionName: Int, optionValue: inout ConsumerInterestCallback) -> Int {
        // function body
        return 0
    }
    
    public func getContextOption(optionName: Int, optionValue: ConsumerContentCallback) -> Int {
        // function body
        return 0
    }
    
    public func getContextOption(optionName: Int, optionValue: inout ConsumerContentCallback) -> Int {
        // function body
        return 0
    }
    
    public func getContextOption(optionName: Int, optionValue: inout ConsumerFullListCallback) -> Int {
        // function body
        return 0
    }

    public func getContextOption(optionName: Int, optionValue: inout Face) -> Int {
        // function body
        return 0
    }
    
    public func getContextOption(optionName: Int, optionValue: inout ConsumerFaceCallback) -> Int {
        // function body
        return 0
    }

}
