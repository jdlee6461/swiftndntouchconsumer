 
//  Consumer.swift
//  SwiftNDNTouchConsumer
//
//  Created by Jongdeog Lee on 11/15/16.
//  Copyright © 2016 Jongdeog Lee. All rights reserved.
//

import Foundation
import SwiftNDNTouch

public class Consumer: Context, FaceDelegate {
    
    /* context inner state variables */
    var m_isRunning:Bool = false
    var m_face:Face!
    var m_dataRetrievalProtocol:DataRetrievalProtocol?
    var m_protocolType = protocolTypes.SDR
    
    var m_prefix:Name?
    var m_suffix:Name?
    var m_forwardingStrategy:Name = fowardingStrategies.BEST_ROUTE! // by default
    
    var m_interestLifetimeMillisec:Int?
    
    var m_minWindowSize:Int?
    var m_maxWindowSize:Int?
    var m_currentWindowSize:Int?
    var m_nMaxRetransmissions:Int?
    var m_nMaxExcludedDigests:Int?
    var m_sendBufferSize:size_t?
    var m_receiveBufferSize:size_t?
    
    var m_isAsync:Bool?
    
    /* selectors */
    
    var m_minSuffixComponents:Int?
    var m_maxSuffixComponents:Int?
    var m_childSelector:Int?
    var m_mustBeFresh:Bool?
    
    /* user-provided callbacks */
    
    var m_onInterestRetransmitted:ConsumerInterestCallback?
    var m_onInterestToLeaveContext:ConsumerInterestCallback?
    var m_onInterestExpired:ConsumerInterestCallback?
    var m_onInterestSatisfied:ConsumerInterestCallback?
    var m_onDataEnteredContext:ConsumerDataCallback?
    var m_onDataToVerify:ConsumerDataVerificationCallback?
    var m_onContentData:ConsumerDataCallback?
    var m_onPayloadReassembled:ConsumerContentCallback?
    var m_onAllListConsumed:ConsumerFullListCallback?
    var m_onFaceOpen:ConsumerFaceCallback?
    var m_onFaceClose:ConsumerFaceCallback?
    var m_onFaceError:ConsumerFaceCallback?
    
    /* 'Exclude' is not implemented yet. It contains a list of full names (string) needs to be excluded */
    var m_excludes: [String]?
    
    public init(prefix: Name, protocols: Int, forwarderIP: String, forwarderPort: UInt16) {
        super.init()
        
        /* Consumer initialization */
        m_isRunning = false
        m_prefix = prefix
        m_interestLifetimeMillisec = defaultValues.INTEREST_LIFETIME
        m_minWindowSize = defaultValues.MIN_WINDOW_SIZE
        m_maxWindowSize = defaultValues.MAX_WINDOW_SIZE
        m_currentWindowSize = -1
        m_nMaxRetransmissions = maxAllowedValues.CONSUMER_MAX_RETRANSMISSIONS
        m_nMaxExcludedDigests = maxAllowedValues.DEFAULT_MAX_EXCLUDED_DIGESTS
        m_isAsync = false
        m_minSuffixComponents = defaultValues.MIN_SUFFIX_COMP
        m_maxSuffixComponents = defaultValues.MAX_SUFFIX_COMP
        m_childSelector = 0
        m_mustBeFresh = false
        
        m_face = Face(delegate: self, host: forwarderIP, port: forwarderPort)
        m_face.open()
        
        if (protocols == protocolTypes.UDR) {
            m_protocolType = protocolTypes.UDR
            m_dataRetrievalProtocol = UnreliableDataRetrieval(consumer: self)
        } else if (protocols == protocolTypes.RDR) {
            m_protocolType = protocolTypes.RDR
            m_dataRetrievalProtocol = ReliableDataRetrieval(consumer: self)
        } else if (protocols == protocolTypes.IDR) {
            m_protocolType = protocolTypes.IDR
            m_dataRetrievalProtocol = InfomaxDataRetrieval(consumer: self)
        } else {
            m_protocolType = protocolTypes.SDR
            m_dataRetrievalProtocol = SimpleDataRetrieval(consumer: self)
        }

    }
    
    deinit {
        stop()
    }
    
    public func consume(suffix: Name) -> Int {
        
        if(self.m_isRunning) {
            /* put in the schedule */
            DispatchQueue.global().async {
                self.postponedConsume(suffix: suffix)
            }

            return miscValues.CONSUMER_BUSY
        }
        
        /* if previously used in non-blocking mode */
        if(self.m_isAsync)! {
            m_dataRetrievalProtocol?.updateFace()
        }
        
        m_suffix = suffix
        m_isAsync = false
        m_dataRetrievalProtocol?.start()
        m_isRunning = false
        return miscValues.CONSUMER_READY
    }
    
    public func consume() -> Int {
        return consume(suffix: Name())
    }
    
    func postponedConsume(suffix: Name) -> Void {
        if(self.m_isAsync)! {
            m_dataRetrievalProtocol?.updateFace()
        }
        
        m_suffix = suffix
        m_isAsync = false
        m_dataRetrievalProtocol?.start()
    }
    
    func asyncConsume(suffix: Name) -> Int {
        if(m_dataRetrievalProtocol?.isRunning())! {
            return miscValues.CONSUMER_BUSY
        }
        
        if !(m_isAsync!) {
            m_dataRetrievalProtocol?.updateFace()
        }
        
        m_suffix = suffix
        m_isAsync = true
        m_dataRetrievalProtocol?.start()
        return miscValues.CONSUMER_READY
    }
    
    func stop() -> Void {
        if let m_dataRetrievalProtocol = self.m_dataRetrievalProtocol {
            if(m_dataRetrievalProtocol.isRunning()) {
                m_dataRetrievalProtocol.stop()
            }
        }
        
        if let m_face = self.m_face {
            if(m_face.isOpen == true) {
                m_face.close()
            }
        }

    }
    
    /* Consumer setter functions */
    
    override public func setContextOption(optionName: Int, optionValue: Int) -> Int {
        switch optionName {
        case defaultValues.MIN_WINDOW_SIZE:
            m_minWindowSize = optionValue
            return optionValues.VALUE_SET
            
        case defaultValues.MAX_WINDOW_SIZE:
            m_maxWindowSize = optionValue
            return optionValues.VALUE_SET
            
        case maxAllowedValues.DEFAULT_MAX_EXCLUDED_DIGESTS:
            m_nMaxExcludedDigests = optionValue
            return optionValues.VALUE_SET
            
        case Options.CURRENT_WINDOW_SIZE:
            m_currentWindowSize = optionValue
            return optionValues.VALUE_SET
            
        case Options.RCV_BUF_SIZE:
            m_receiveBufferSize = optionValue
            return optionValues.VALUE_SET
            
        case Options.SND_BUF_SIZE:
            m_sendBufferSize = optionValue
            return optionValues.VALUE_SET
            
        case Options.INTEREST_RETX:
            if (optionValue < maxAllowedValues.CONSUMER_MAX_RETRANSMISSIONS) {
                m_nMaxRetransmissions = optionValue
                return optionValues.VALUE_SET
            } else {
                return optionValues.VALUE_NOT_SET
            }
            
        case Options.INTEREST_LIFETIME:
            m_interestLifetimeMillisec = optionValue
            return optionValues.VALUE_SET
            
        case Options.MIN_SUFFIX_COMP_S:
            if (optionValue >= 0) {
                m_minSuffixComponents = optionValue
                return optionValues.VALUE_SET
            } else {
                m_minSuffixComponents = defaultValues.MIN_SUFFIX_COMP
                return optionValues.VALUE_NOT_SET
            }
        
        case Options.MAX_SUFFIX_COMP_S:
            if (optionValue >= 0) {
                m_maxSuffixComponents = optionValue
                return optionValues.VALUE_SET
            } else {
                m_maxSuffixComponents = defaultValues.MAX_SUFFIX_COMP
                return optionValues.VALUE_NOT_SET
            }
            
        case Options.RIGHTMOST_CHILD_S:
            if (optionValue == 1) {
                m_childSelector = 1
            } else {
                m_childSelector = 0
            }
            return optionValues.VALUE_SET
            
        case Options.LEFTMOST_CHILD_S:
            if (optionValue == 1) {
                m_childSelector = 0
            } else {
                m_childSelector = 1
            }
            return optionValues.VALUE_SET
            
        case Options.INTEREST_RETRANSMIT:
            if (optionValue == EMPTY_CALLBACK) {
                m_onInterestRetransmitted = {_,_ in }
                return optionValues.VALUE_SET
            } else {
                return optionValues.VALUE_NOT_SET
            }
            
        case Options.INTEREST_EXPIRED:
            if (optionValue == EMPTY_CALLBACK) {
                m_onInterestExpired = {_,_ in }
                return optionValues.VALUE_SET
            } else {
                return optionValues.VALUE_NOT_SET
            }
            
        case Options.INTEREST_SATISFIED:
            if (optionValue == EMPTY_CALLBACK) {
                m_onInterestSatisfied = {_,_ in }
                return optionValues.VALUE_SET
            } else {
                return optionValues.VALUE_NOT_SET
            }
            
        case Options.INTEREST_LEAVE_CNTX:
            if (optionValue == EMPTY_CALLBACK) {
                m_onInterestToLeaveContext = {_,_ in }
                return optionValues.VALUE_SET
            } else {
                return optionValues.VALUE_NOT_SET
            }
            
        case Options.DATA_ENTER_CNTX:
            if (optionValue == EMPTY_CALLBACK) {
                m_onDataEnteredContext = {_,_ in }
                return optionValues.VALUE_SET
            } else {
                return optionValues.VALUE_NOT_SET
            }
            
        case Options.DATA_TO_VERIFY:
            if (optionValue == EMPTY_CALLBACK) {
                m_onDataToVerify = {_,_ in return false}
                return optionValues.VALUE_SET
            } else {
                return optionValues.VALUE_NOT_SET
            }
            
        case Options.CONTENT_RETRIEVED:
            if (optionValue == EMPTY_CALLBACK) {
                m_onPayloadReassembled = {_,_ in }
                return optionValues.VALUE_SET
            } else {
                return optionValues.VALUE_NOT_SET
            }
        
        default:
            return optionValues.VALUE_NOT_SET
        }
    }
    
    override public func setContextOption(optionName: Int, optionValue: Bool) -> Int {
        switch optionName {
            
        case Options.MUST_BE_FRESH_S:
            m_mustBeFresh = optionValue
            return optionValues.VALUE_SET
            
        case Options.RIGHTMOST_CHILD_S:
            if (optionValue == true) {
                m_childSelector = 1
            } else {
                m_childSelector = 0
            }
            return optionValues.VALUE_SET
            
        case Options.LEFTMOST_CHILD_S:
            if (optionValue == true) {
                m_childSelector = 0
            } else {
                m_childSelector = 1
            }
            return optionValues.VALUE_SET
            
        default:
            return optionValues.VALUE_NOT_SET
        }
    }
    
    override public func setContextOption(optionName: Int, optionValue: Name) -> Int {
        switch optionName {
            
        case Options.PREFIX:
            m_prefix = optionValue
            return optionValues.VALUE_SET
        
        case Options.SUFFIX:
            m_suffix = optionValue
            return optionValues.VALUE_SET
            
        default:
            return optionValues.VALUE_NOT_SET
        }
    }
    
    override public func setContextOption(optionName: Int, optionValue: @escaping ConsumerDataCallback) -> Int {
        switch optionName {
            
        case Options.DATA_ENTER_CNTX:
            m_onDataEnteredContext = optionValue
            return optionValues.VALUE_SET
            
        default:
            return optionValues.VALUE_NOT_SET
        }
    }
    
    override public func setContextOption(optionName: Int, optionValue: @escaping ConsumerDataVerificationCallback) -> Int {
        switch optionName {
            
        case Options.DATA_TO_VERIFY:
            m_onDataToVerify = optionValue
            return optionValues.VALUE_SET
            
        default:
            return optionValues.VALUE_NOT_SET
        }
    }
    
    override public func setContextOption(optionName: Int, optionValue: @escaping ConsumerInterestCallback) -> Int {
        switch optionName {
            
        case Options.INTEREST_RETRANSMIT:
            m_onInterestRetransmitted = optionValue
            return optionValues.VALUE_SET
            
        case Options.INTEREST_LEAVE_CNTX:
            m_onInterestToLeaveContext = optionValue
            return optionValues.VALUE_SET
            
        case Options.INTEREST_EXPIRED:
            m_onInterestExpired = optionValue
            return optionValues.VALUE_SET
            
        case Options.INTEREST_SATISFIED:
            m_onInterestSatisfied = optionValue
            return optionValues.VALUE_SET
            
        default:
            return optionValues.VALUE_NOT_SET
        }
    }
    
    override public func setContextOption(optionName: Int, optionValue: @escaping ConsumerContentCallback) -> Int {
        switch optionName {
            
        case Options.CONTENT_RETRIEVED:
            m_onPayloadReassembled = optionValue
            return optionValues.VALUE_SET
        
        default:
            return optionValues.VALUE_NOT_SET
        }
    }
    
    override public func setContextOption(optionName: Int, optionValue: @escaping ConsumerFullListCallback) -> Int {
        switch optionName {
            
        case Options.ALL_LIST_CONSUMED:
            m_onAllListConsumed = optionValue
            return optionValues.VALUE_SET
            
        default:
            return optionValues.VALUE_NOT_SET
        }
    }
    
    override public func setContextOption(optionName: Int, optionValue: @escaping ConsumerFaceCallback) -> Int {
        switch optionName {
            
        case Options.ON_OPEN:
            m_onFaceOpen = optionValue
            return optionValues.VALUE_SET
        case Options.ON_CLOSE:
            m_onFaceClose = optionValue
            return optionValues.VALUE_SET
        case Options.ON_ERROR:
            m_onFaceError = optionValue
            return optionValues.VALUE_SET
        default:
            return optionValues.VALUE_NOT_SET
        }
    }
    
    /* Temporary setter for my excludes */
    public func setContextOption(optionName: Int, optionValue: [String]) -> Int {
        switch optionName {
            
        case Options.EXCLUDE_S:
            m_excludes = optionValue
            return optionValues.VALUE_SET
            
        default:
            return optionValues.VALUE_NOT_SET
        }
    }
    
    /* Consumer getter functions */
    
    override public func getContextOption(optionName: Int, optionValue: inout Int) -> Int {
        switch optionName {
            
        case Options.MIN_WINDOW_SIZE:
            optionValue = m_minWindowSize!
            return optionValues.FOUND
            
        case Options.MAX_WINDOW_SIZE:
            optionValue = m_maxWindowSize!
            return optionValues.FOUND
            
        case Options.MAX_EXCLUDED_DIGESTS:
            optionValue = m_nMaxExcludedDigests!
            return optionValues.FOUND
            
        case Options.CURRENT_WINDOW_SIZE:
            optionValue = m_currentWindowSize!
            return optionValues.FOUND
            
        case Options.RCV_BUF_SIZE:
            optionValue = m_receiveBufferSize!
            return optionValues.FOUND
            
        case Options.SND_BUF_SIZE:
            optionValue = m_sendBufferSize!
            return optionValues.FOUND
            
        case Options.INTEREST_RETX:
            optionValue = m_nMaxRetransmissions!
            return optionValues.FOUND
            
        case Options.INTEREST_LIFETIME:
            optionValue = m_interestLifetimeMillisec!
            return optionValues.FOUND
            
        case Options.MIN_SUFFIX_COMP_S:
            optionValue = m_minSuffixComponents!
            return optionValues.FOUND
            
        case Options.MAX_SUFFIX_COMP_S:
            optionValue = m_maxSuffixComponents!
            return optionValues.FOUND
            
        case Options.RIGHTMOST_CHILD_S:
            optionValue = m_childSelector!
            return optionValues.FOUND
            
        default:
            return optionValues.NOT_FOUND
        }
    }
    
    override public func getContextOption(optionName: Int, optionValue: inout Bool) -> Int {
        switch optionName {
            
        case Options.MUST_BE_FRESH_S:
            optionValue = m_mustBeFresh!
            return optionValues.FOUND
        
        case Options.ASYNC_MODE:
            optionValue = m_isAsync!
            return optionValues.FOUND
        
        case Options.RUNNING:
            optionValue = m_isRunning
            return optionValues.FOUND
            
        default:
            return optionValues.NOT_FOUND
        }
    }
    
    override public func getContextOption(optionName: Int, optionValue: inout Face) -> Int {
        switch optionName {
            
        case Options.FACE:
            if let face = m_face {
                optionValue = face
                return optionValues.FOUND
            } else {
                return optionValues.NOT_FOUND
            }
            
        default:
            return optionValues.NOT_FOUND
        }
    }
    
    override public func getContextOption(optionName: Int, optionValue: inout Name) -> Int {
        switch optionName {
            
        case Options.PREFIX:
            optionValue = m_prefix!
            return optionValues.FOUND
        
        case Options.SUFFIX:
            optionValue = m_suffix!
            return optionValues.FOUND
            
        case Options.FORWARDING_STRATEGY:
            optionValue = m_forwardingStrategy
            return optionValues.FOUND
            
        default:
            return optionValues.NOT_FOUND
        }
    }
    
    override public func getContextOption(optionName: Int, optionValue: inout ConsumerDataCallback) -> Int {
        switch optionName {
        case Options.DATA_ENTER_CNTX:
            if let callback = m_onDataEnteredContext {
                optionValue = callback
                return optionValues.FOUND
            } else {
                return optionValues.NOT_FOUND
            }
            
        default:
            return optionValues.NOT_FOUND
        }
    }
    
    override public func getContextOption(optionName: Int, optionValue: inout ConsumerDataVerificationCallback) -> Int {
        switch optionName {
            
        case Options.DATA_TO_VERIFY:
            if let callback = m_onDataToVerify {
                optionValue = callback
                return optionValues.FOUND
            } else {
                return optionValues.NOT_FOUND
            }
            
        default:
            return optionValues.NOT_FOUND
        }
    }
    
    override public func getContextOption(optionName: Int, optionValue: inout ConsumerInterestCallback) -> Int {
        switch optionName {
            
        case Options.INTEREST_RETRANSMIT:
            if let callback = m_onInterestRetransmitted {
                optionValue = callback
                return optionValues.FOUND
            } else {
                return optionValues.NOT_FOUND
            }
        
        case Options.INTEREST_LEAVE_CNTX:
            if let callback = m_onInterestToLeaveContext {
                optionValue = callback
                return optionValues.FOUND
            } else {
                return optionValues.NOT_FOUND
            }
            
        case Options.INTEREST_EXPIRED:
            if let callback = m_onInterestExpired {
                optionValue = callback
                return optionValues.FOUND
            } else {
                return optionValues.NOT_FOUND
            }
            
        case Options.INTEREST_SATISFIED:
            if let callback = m_onInterestSatisfied {
                optionValue = callback
                return optionValues.FOUND
            } else {
                return optionValues.NOT_FOUND
            }
            
        default:
            return optionValues.NOT_FOUND
        }
    }

    override public func getContextOption(optionName: Int, optionValue: inout ConsumerContentCallback) -> Int {
        switch optionName {
            
        case Options.CONTENT_RETRIEVED:
            if let callback = m_onPayloadReassembled {
                optionValue = callback
                return optionValues.FOUND
            } else {
                return optionValues.NOT_FOUND
            }
            
        default:
            return optionValues.NOT_FOUND
        }
    }
    
    override public func getContextOption(optionName: Int, optionValue: inout ConsumerFullListCallback) -> Int {
        switch optionName {
            
        case Options.ALL_LIST_CONSUMED:
            if let callback = m_onAllListConsumed {
                optionValue = callback
                return optionValues.FOUND
            } else {
                return optionValues.NOT_FOUND
            }
            
        default:
            return optionValues.NOT_FOUND
        }
    }
    
    override public func getContextOption(optionName: Int, optionValue: inout ConsumerFaceCallback) -> Int {
        switch optionName {
            
        case Options.ON_OPEN:
            if let callback = m_onFaceOpen {
                optionValue = callback
                return optionValues.FOUND
            } else {
                return optionValues.NOT_FOUND
            }
        case Options.ON_CLOSE:
            if let callback = m_onFaceClose {
                optionValue = callback
                return optionValues.FOUND
            } else {
                return optionValues.NOT_FOUND
            }
        case Options.ON_ERROR:
            if let callback = m_onFaceError {
                optionValue = callback
                return optionValues.FOUND
            } else {
                return optionValues.NOT_FOUND
            }
        default:
            return optionValues.NOT_FOUND
        }
    }
    
    /* Temporary getter for my excludes */
    public func getContextOption(optionName: Int, optionValue: inout [String]) -> Int {
        switch optionName {
            
        case Options.EXCLUDE_S:
            if let excludes = m_excludes {
                optionValue = excludes
                return optionValues.FOUND
            } else {
                return optionValues.NOT_FOUND
            }
            
        default:
            return optionValues.NOT_FOUND
        }
    }
    
    /* Face Delegate Functions */
    public func onOpen() {
        if let callback = m_onFaceOpen {
            callback()
        } else {
            print("Face open")
        }
    }
    
    public func onClose() {
        if let callback = m_onFaceClose {
            callback()
        } else {
            print("Face close")
        }
    }
    
    public func onError(_ reason: String) {
        if let callback = m_onFaceError {
            callback()
        } else {
            print("Face error: "+reason)
        }
    }
    
}
