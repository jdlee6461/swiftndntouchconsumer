//
//  SimpleDataRetrieval.swift
//  SwiftNDNTouchConsumer
//
//  Created by Jongdeog Lee on 11/28/16.
//  Copyright © 2016 Jongdeog Lee. All rights reserved.
//

import Foundation
import SwiftNDNTouch

class SimpleDataRetrieval: DataRetrievalProtocol {
    
    public override init(consumer: Consumer) {
        super.init(consumer: consumer)
        consumer.getContextOption(optionName: Options.FACE, optionValue: &m_face!)
    }
    
    public override func start() {
        m_isRunning = true
        sendInterest()
    }
    
    public override func stop() {
        m_isRunning = false
    }
    
    
    func onData(interest: Interest, data: SwiftNDNTouch.Data) -> Void {
        if (m_isRunning == false) {
            return
        }
        
        var onDataEnteredContext: ConsumerDataCallback = {_,_ in }

        if (m_consumer?.getContextOption(optionName: Options.DATA_ENTER_CNTX, optionValue: &onDataEnteredContext) == optionValues.FOUND) {
            onDataEnteredContext(m_consumer!, data)
        }
        
        var onInterestSatisfied: ConsumerInterestCallback = {_,_ in }
        
        if (m_consumer?.getContextOption(optionName: Options.INTEREST_SATISFIED, optionValue: &onInterestSatisfied) == optionValues.FOUND) {
            onInterestSatisfied(m_consumer!, interest)
        }
        
        var onDataToVerify: ConsumerDataVerificationCallback = {_,_ in return false}
        
        if (m_consumer?.getContextOption(optionName: Options.DATA_TO_VERIFY, optionValue: &onDataToVerify) == optionValues.FOUND) {
            if (onDataToVerify(m_consumer!, data) == true) {
                let content = data.getContent()
                
                var onPayload: ConsumerContentCallback?
                if (m_consumer?.getContextOption(optionName: Options.CONTENT_RETRIEVED, optionValue: &onPayload!) == optionValues.FOUND) {
                    onPayload!(m_consumer!, content)
                }
            }
        } else {
            let content = data.getContent()
            
            var onPayload: ConsumerContentCallback = {_,_ in}
            if (m_consumer?.getContextOption(optionName: Options.CONTENT_RETRIEVED, optionValue: &onPayload) == optionValues.FOUND) {
                onPayload(m_consumer!, content)
            }
        }
        
        m_isRunning = false
        
    }
    
    func onTimeout(interest: Interest) -> Void {
        if(m_isRunning == false) {
            return
        }
        
//        var onInterestExpired: ConsumerInterestCallback?
//        m_consumer?.getContextOption(optionName: Options.INTEREST_EXPIRED, optionValue: onInterestExpired!)
//        if (onInterestExpired != nil) {
//            onInterestExpired!(m_consumer!, interest)
//        }
        
        m_isRunning = false
    }
    
    func sendInterest() -> Void {
        var prefix: Name = Name()
        m_consumer?.getContextOption(optionName: Options.PREFIX, optionValue: &prefix)
        
        var suffix: Name = Name()
        m_consumer?.getContextOption(optionName: Options.SUFFIX, optionValue: &suffix)
        
        if (suffix.isEmpty == false) {
            prefix = Name(url: prefix.toUri()+suffix.toUri())!
        }
        
        let interest = Interest()
        interest.name = prefix
        var interestLifetime = 0
        m_consumer?.getContextOption(optionName: Options.INTEREST_LIFETIME, optionValue: &interestLifetime)
        interest.setInterestLifetime(UInt64(interestLifetime)) // milliseconds
        interest.setMustBeFresh()
        
        var onInterestToLeaveContext: ConsumerInterestCallback = {_,_ in }
        
        m_consumer?.getContextOption(optionName: Options.INTEREST_LEAVE_CNTX, optionValue: &onInterestToLeaveContext)
        
        if (onInterestToLeaveContext != nil) {
            onInterestToLeaveContext(m_consumer!, interest)
        }

        m_face?.expressInterest(interest, onData: onData, onTimeout: onTimeout)
        
    }
}
