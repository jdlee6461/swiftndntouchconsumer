#
# Be sure to run `pod lib lint SwiftNDNTouchConsumer.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SwiftNDNTouchConsumer'
  s.version          = '0.2.1'
  s.summary          = 'This is a NDN consumer model designed for NDN-iOS developers.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
This implementation is based on Consumer-Producer APIs designed by UCLA developers (https://github.com/named-data/Consumer-Producer-API). Especially, this code include consumer APIs and it is written in Swift3 to be compatible with iOS platforms (above 10.0). Currently, the model includes 4 data retrieval protocols (SDR, UDR, RDR, IDR). The example project uses IDR(Infomax Data Retreival) to transmit image files in the least redundant order.
                       DESC

  s.homepage         = 'https://bitbucket.org/jdlee6461/swiftndntouchconsumer'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Jongdeog Lee' => 'jlee700@illinois.edu' }
  s.source           = { :git => 'https://bitbucket.org/jdlee6461/swiftndntouchconsumer', :tag => s.version.to_s }
  # s.source           = { :path => '/Users/Jongdeog/Documents/SwiftNDNTouchConsumer' }

  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

  s.source_files = 'SwiftNDNTouchConsumer/Classes/**/*'
  
  # s.resource_bundles = {
  #   'SwiftNDNTouchConsumer' => ['SwiftNDNTouchConsumer/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'SwiftNDNTouch', '~> 0.2.1'
end
